#!/bin/bash -e

interface=$(ip -o -f inet addr show | grep wl | awk '/scope global/ {print $2}' | head -n 1)

exec /pirogue-flow-inspector $interface "$@"
