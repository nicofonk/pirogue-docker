#!/bin/bash -e

interface=$(ip -o -f inet addr show | grep wl | awk '/scope global/ {print $2}' | head -n 1)
mask=$(ip -o -f inet addr show $interface | awk '/scope global/ {print $4}')
sed -e 's!WLAN_IFACE!'${interface}'!g' -e 's!HOME_NET: "\[10.8.0.0/24\]"!HOME_NET: "\['${mask}'\]"!g' -e 's!/run/suricata.socket!/run/suricata/suricata.socket!g' /usr/share/pirogue/suricata/suricata.yaml > /etc/suricata/suricata.yaml
suricata-update --no-check-certificate --no-test

exec "$@"
